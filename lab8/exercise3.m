clear
close all
load char_e; %load character E
figure(4); subplot(2,2,1); imshow(char_e); %and display it
[x y]=perim_sort(char_e); %extract and sort the boundary coordinates
f=x+1i*y; %make a complex 1D signal out of the coordinates
N=length(f); %number of boundary points (N=262)
figure(4); subplot(2,2,2); plot(imag(f),real(f)); %plot the boundary
F=fft(f,N); %Fdescriptors of the boundary
fi=ifft(F,N); %go back to boundary points x+i*y
figure(4); subplot(2,2,3); plot(imag(fi),real(fi)); % and plot the boundaryeconstructed boundary

% the Fd:s should be put in in pair, i.e. F(k) and F(-k) is a pair
% in the Brick-wall Fourier domain filter
CC=zeros(N,1); %start with a zero filter
CC(mod([0:29],N)+1)=ones(30,1); %add in ones in pair; F(k)
CC(mod([-29:-1],N)+1)=ones(29,1); %F(-k)
FC=F.*CC; % filter
% FC = F;
% FC(1) = 0;
f1=ifft(FC,N); % reconstruct the boundary from the low order Fd:s
figure(4); subplot(2,2,4); plot(imag(f1),real(f1)); %reconstructed boundary