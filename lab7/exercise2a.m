clear
close all
B = [1, 1.5, 3.5];
for i = 1:3
    [magn, argument]=fmtest(256,[0.1,0.33]*pi, 0.75);
    LS=linsymexer(magn); %estimate the linear symmetry (LS)
    figure(i)
    lsdisp(LS, B(i)); truesize; %and display (give B a value!)
end