clear
close all
fim=double(imread('wood+chain.tif'));
figure(11);
subplot(2,2,1);imagesc(fim); colormap(gray); axis image;
subplot(2,2,2); lsdisp(linsymexer(fim,[0.4 4]),.2); axis image;
subplot(2,2,3); lsdisp(linsymexer(fim,[6 10]),.5); axis image