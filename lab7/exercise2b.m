clear
close all
fim=double(imread('fingerp.tif','tif')); %read the image
figure(1);
imagesc(fim);colormap(gray);axis image; %and display it
LS2=linsymexer(fim,[1.5,2]); %estimate LS
figure(2);
lsdisp(LS2,0.5); truesize; %and display

mnt=LS2(:,:,3)-LS2(:,:,2);
figure(3);
lsdisp(fim.*exp(1i*7*mnt)); truesize;
% figure(4);
% hist(mnt(:))

% for i = 0.1:.1:10*pi
%     figure(5);
%     clf();
%     lsdisp(fim.*exp(1i*i*mnt)); truesize;
%     pause(0.1);
% end