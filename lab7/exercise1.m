A = [1, 0.75, 0.1];
[magn, argument]=fmtest(256,[0.1,0.33]*pi, A(1)); %give A a value!
for i=1:3
    [magn, argument]=fmtest(256,[0.1,0.33]*pi, A(i)); %give A a value!
    figure(2*i-1);
    imagesc(magn); colormap(gray); truesize; %display the magnitude image
    figure(2*i);
    imagesc(argument); colormap(gray); truesize;
end

magnone=ones(256,256); %1-magnitude image
im1_c=magnone.*exp(1i*2*argument); %complex image
figure(17); lsdisp(im1_c); truesize; %color code for local orientations