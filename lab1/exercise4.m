clear all
close all

B=imread('pout.tif'); %load the image
B=double(B); %datatype float
g=B(80:100,108:128); %left eye as the matching pattern 21 x 21

N=(imnoise(B/255,'gaussian',0,0.01)).*255; %add gaussian noise, mean=0, var=0.001
S=exsim(N,g); %calculate the similarity map