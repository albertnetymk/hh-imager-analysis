% hermsym.m  /kn 00-02-04
% hermsym.m  /jb 10-08-16
% reconstruct the image from half the spectrum
% using Hermitian symmetry
clear all
close all
% f=imread('blood1.tif'); % read image
f=imread('cameraman.tif'); % read image
f=double(f(1:256,1:256));

c=fft2(f); % calculate FT

c1=c; % reconstructed FT 

%reconstruct the FT from one half
kcol=0:128;  
krow=0:255;
c1(mod(krow,256)+1,mod(-kcol,256)+1)=0; % We destroy the information in one half FT (left)
c1(mod(krow,256)+1,mod(-kcol,256)+1)=conj(c(mod(-krow,256)+1,mod(kcol,256)+1)); %Copy half of FT (right) onto the other half (right)
f1 = rot90(f,2);
h=fft2(c1); % image from the reconstructed FT

rotated = zeros(size(h));
rotated(1:size(rotated, 1)-1, 1:size(rotated, 2)-1) = h(2:size(h,1), 2:size(h,2));
rotated(size(rotated,1), 1:end-1) = h(1, 2:end);
rotated(1:end-1, size(rotated,2)) = h(2:end, 1);
rotated(end) = h(1);
rotated = rotated/256^2;
g=ifft2(c1); % image from the reconstructed FT

% error calculation
e=f-g; % original - reconstructed
error=sum(sum(e.*e)) % sum of square error

% display
colormap(gray);
figure(1)
clf()
subplot(2,2,1); imagesc(f); title('original image');
subplot(2,2,2); imagesc(f1); title('rotate 180 degrees');
subplot(2,2,3);imagesc(real(g));title('reconstructed image');
subplot(2,2,4);imagesc(abs(rotated));title('double fft image');
% e=abs(rotated) - f1;
% error=sum(sum(e.*e)) % sum of square error
% figure(2)
% clf()
% [X, Y] = meshgrid(1:size(g, 1), 1:size(g,2));
% subplot(2,1,1);mesh(X, Y, abs(c1))
% subplot(2,1,2);mesh(X, Y, abs(fftshift(c1)))
% grid on
% figure(3)
% clf()
% colormap(gray);
% subplot(2,2,1); imagesc(f); title('original image');
% subplot(2,2,2); imagesc(abs(c1)); title('abs');
% subplot(2,2,3);imagesc(log(abs(c1)));title('log');
% subplot(2,2,4);imagesc(log(abs(fftshift(c1))));title('fftshift');
% figure(4)
% clf()
% E = base(256, 5, -10);
% proj = sum(sum(f.*conj(E)))
% c(mod(5,256)+1,mod(-10,256)+1)
