clear
close all

load char_e;

img= char_e;
[Re,xe,ye]=FD_contour(img,1); %compute ref vector and display contour images in Fig. 1



load char_w; %load character W
[Rw,xw,yw]=FD_contour(char_w,2); %compute ref vector and contour images , and display

img=imresize(img,1.6,'bicubic'); %Enlarge E 1.6 times
[Se,sxe,sye]=FD_contour(img,3); %compute ref vector and contour points, and display
Se %print the feature vector on screen

dist_e=norm(Se-Re); %distance between scaled character E and reference E
dist_w=norm(Se-Rw); %distance between scaled character E and reference W
L=dist_w/dist_e %Likelihood of img being E compared to being W