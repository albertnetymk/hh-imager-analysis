clear
close all

load char_e;

img= char_e;
[Re,xe,ye]=FD_contour(img,1); %compute ref vector and display contour images in Fig. 1

load char_w; %load character W
[Rw,xw,yw]=FD_contour(char_w,2); %compute ref vector and contour images , and display

img=imresize(img,1.6,'bicubic'); %Enlarge E 1.6 times
img2=imrotate(img,60,'bicubic');
zs=zeros(round(1.5*size(img2))); %create a larger image
zs(32:(size(img2,1)+31),15:(size(img2,2)+14))=img2; %Translate the E character with ?x=15 and ?y=32
img3=zs;
[SRTe,x3re,y3re]=FD_contour(img3,5); %compute ref vector and contour points, and display


dist_e=norm(SRTe-Re) %distance between scaled character E and reference E
dist_w=norm(SRTe-Rw) %distance between scaled character E and reference W
L=dist_w/dist_e %Likelihood of img being E compared to being W