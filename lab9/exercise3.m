clear
close all

load char_e;

img= char_e;
[Re,xe,ye]=FD_contour(img,1); %compute ref vector and display contour images in Fig. 1

load char_w; %load character W
[Rw,xw,yw]=FD_contour(char_w,2); %compute ref vector and contour images , and display

img=imresize(img,1.6,'bicubic'); %Enlarge E 1.6 times
img2=imrotate(img,60,'bicubic'); %rotate img (scaled E) with 60 degrees
[SRe,x3re,y3re]=FD_contour(img2,4); %compute ref vector and contour points, and display

dist_e=norm(SRe-Re); %distance between scaled character E and reference E
dist_w=norm(SRe-Rw); %distance between scaled character E and reference W
L=dist_w/dist_e %Likelihood of img being E compared to being W