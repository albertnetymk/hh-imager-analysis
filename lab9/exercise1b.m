clear
close all
load char_e; %load character E
img= char_e;
[Re,xe,ye]=FD_contour(img,1); %compute ref vector and display contour images in Fig. 1
Re %print the reference vector on screen

load char_w; %load character W
[Rw,xw,yw]=FD_contour(char_w,2); %compute ref vector and contour images , and display
Rw %print the reference vector on screen