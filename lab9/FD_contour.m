function [fv, xe, ye] = FD_contour(im, fig)
    b_cell=bwboundaries(im); % extract the contour points. see help bwboundaries for output.

    xe= b_cell{1}(:,2); %there is one letter, hence one cell, which is a matrix. Pick col 2 for x coords.
    ye= -b_cell{1}(:,1); % ... Pick col 1 for y coords and change sign.
    figure(fig);plot(xe, ye);
    fe = xe + 1i*ye;
    Fe = fft(fe);
    
    fv = normFD(Fe,[-4, -3, -2, -1, 2, 3, 4]);
end