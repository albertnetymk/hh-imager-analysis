clear
close all
L=zeros(64,64);
SIZE = 2;
center = floor(size(L)/2);
% center = [SIZE+1, SIZE+1];
L((center(1)-SIZE):(center(2)+SIZE), ...
  (center(1)-SIZE):(center(2)+SIZE)) ...
  =ones(SIZE*2+1,SIZE*2+1); %put in a white square 7 x 7
subplot(2,2,1); imshow(L); %show it
FL=fft2(L); %2D DFT
subplot(2,2,2); imshow(fftshift(log10(abs(FL)))); %show the centralized DFT