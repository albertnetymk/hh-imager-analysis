clear
close all
f = ones(5,5)
g = ones(3,3)
y=conv2(f,g);
%do convolution by DFT, multiplication, and IDFT
F=fft2(f,5,5); % DFT in 5 x 5 points (N=5)
G=fft2(g,5,5); % both DFT must be of equal size (later on multiplication!)
% F=fft2(f,7,7); % DFT in 5 x 5 points (N=5)
% G=fft2(g,7,7); % both DFT must be of equal size (later on multiplication!)
% F=fft2(f,8,8); % DFT in 5 x 5 points (N=5)
% G=fft2(g,8,8); % both DFT must be of equal size (later on multiplication!)

Y=F.*G; % point multiplication
yy=real(ifft2(Y)); % convolved image
% figure(1);clf();
% subplot(2,1,1);imshow(y)
% subplot(2,1,2);imshow(yy)
y
format short; round(yy)

F=fft2(f,7,7); % DFT in 5 x 5 points (N=5)
G=fft2(g,7,7); % both DFT must be of equal size (later on multiplication!)
% F=fft2(f,8,8); % DFT in 5 x 5 points (N=5)
% G=fft2(g,8,8); % both DFT must be of equal size (later on multiplication!)

Y=F.*G; % point multiplication
yy=real(ifft2(Y)); % convolved image
format short; round(yy)
