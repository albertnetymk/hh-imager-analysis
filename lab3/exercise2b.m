clear
close all
f=imread('flowers.jpg');
f=double(f(1:256,1:256));
g=ones(21,21)/(21*21);
y=conv2(f,g);
F = fft2(f, 256, 256);
G = fft2(g, 256, 256);
yy1 = real(ifft2(F.*G));

s = 276;
F = fft2(f, s, s);
G = fft2(g, s, s);
yy2 = real(ifft2(F.*G));
figure(1)
subplot(2,2,1); imshow(f/255);
subplot(2,2,2); imshow(y/255);
subplot(2,2,3); imshow(yy1/255);
subplot(2,2,4); imshow(yy2/255);
new = yy2(11:266, 11:266);
figure(2)
imshow(new/255);
e = new - yy1;
sum(sum(e.^2))