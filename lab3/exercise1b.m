clear
close all
d=circle(20); %generate a white circle on a black background
d = double(d);
g = ones(8,8)/8^2;
new = conv2(d,g, 'same');
NEW = fft2(new);
D=fft2(d); %2D DFT
figure(1)
[row col] = size(d);
radius = 30;
center_D = fftshift(D);
center_D = center_D((row/2-radius):(row/2+radius), (col/2-radius):(col/2+radius));

center_N = fftshift(NEW);
center_N = center_N((row/2-radius):(row/2+radius), (col/2-radius):(col/2+radius));
subplot(2,2,1); imshow(d); %show the circle
subplot(2,2,3); imagesc(fftshift(log10(abs(D))));  axis image %and its DFT
subplot(2,2,4); imagesc(fftshift(log10(abs(NEW)))); axis image %and its DFT


colormap('default');
figure(2)
subplot(2,1,1); surf(log10(abs(center_D)));
subplot(2,1,2); surf(log10(abs(center_N)));