clear
close all
[magn, argument]=fmtest(256,[0.1,0.33]*pi, 1); %generate testimage
figure(1); imagesc(magn); colormap(gray(256)); truesize; %display
figure(2); imagesc(argument); colormap(gray(256)); truesize;

magnone=ones(256,256); %constant image=1
%display a three component real valued image im_r
im_r(:,:,1)=argument; %steers H,
im_r(:,:,2)=magnone; %steers V,
im_r(:,:,3)=ones(256,256); %steers S (is put to 1)
figure(3); lsdisp(im_r); truesize;

%display a complex image im_c
%angle(im_c) steers H, abs(im_c) steers V,
%and S is put to 1 in the lsdip function
im_c=magnone.*exp(1i*argument); %make a complex image;
figure(4); lsdisp(im_c); truesize; %and display it

arg2=mod(2*argument,2*pi);
Im2Arg1=magnone.*exp(i*arg2); %generate �2*argument� image
figure(5); lsdisp(Im2Arg1); truesize;
figure(6); imagesc(arg2); colormap(gray(256)); truesize;

% In figure(17) any two points that are symmetric with respect to origin have the same color!
% Why? (Hint: write down the 2*argument of the symmetric points).
% Modulate now magn with the 2*argument via lsdisp.
Im2Arg=magn.*exp(i*2*argument); %generate �2*argument� image
figure(7); lsdisp(Im2Arg) ;truesize;
