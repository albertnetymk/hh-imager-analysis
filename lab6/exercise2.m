clear
close all

[magn, argument]=fmtest(256,[0.1,0.33]*pi, 1); %generate testimage

grdient=showex_gr(magn); %shows gradient in image magn
% Do local averaging in the gradient image by using a large gaussian with ?=2.5.

std=2.5;
x=-round(3*std):round(3*std);
gx=exp(-(x.*x)/2/std/std);
gx=gx/sum(gx); %in the x direction
gy=gx'; %and in the y direction
%do the local averaging on the gradient image
avg_grad=filter2(gy,filter2(gx,grdient));
%display the avg_grad image by using lsdisp and option complex input
figure(51);lsdisp(avg_grad,0.5,'sclon'); truesize;


orientation=showex_gr(magn); %shows orientations in image magn
avg_or=filter2(gy,filter2(gx,orientation)); %do local averaging
figure(52); lsdisp(avg_or); truesize; %and display