clear
% close all

s=1.4; %standard deviation
[x,y]=meshgrid(-round(3*s):round(3*s),-round(3*s):round(3*s)); %sample grid
g=exp(-(x.*x + y.*y)/(2*s*s)); %2D smoothing filter
g=g/sum(sum(g)); % sum of weights equals one
figure(1); subplot(2,1,1);mesh(x,y,g); %show the filter
x1=-round(3*s):round(3*s); %sample grid
gx= exp(-(x1.*x1 )/(2*s*s)); %smoothing filter in the x?direction
gx=gx/sum(gx); % sum of weights equals one
gy=gx'; %smoothing filter in the y?direction (transpose of gx)
figure(1); subplot(2,1,2); stem(x1,gx); %show the 1D smoothing filter

f=double(imread('flowers.jpg')); %load the image
f=f(1:256,1:256); %crop it to 256 x 256 pixels
ff = imnoise(f/255, 'gaussian', 0, 0.001)*255;
backup_f = f;
f = ff;
y=conv2(f,g,'valid'); %filter the image
figure(2);clf()
subplot(2,2,1); imshow(f/255); % original image
subplot(2,2,2); imshow(y/255); % smoothed image the 1D smoothing filter

yy=conv2(conv2(f,gx,'valid'),gy,'valid'); %filter the image
subplot(2,2,3);imshow(yy/255); %and display it
e=y-yy; %compute error
sum(sum(e.*e)) %print it on screen

Gx=fft(gx,32); %DFT of the filter
subplot(2,2,4);
stem(-16:15,fftshift(abs(Gx))); %display the magnitude spectrum

s=2.4; %standard deviation
[x,y]=meshgrid(-round(3*s):round(3*s),-round(3*s):round(3*s));
g=exp(-(x.*x + y.*y)/(2*s*s)); %2D smoothing filter
g=g/sum(sum(g)); % sum of weights equals one
x1=-round(3*s):round(3*s); %sample grid
gx= exp(-(x1.*x1 )/(2*s*s)); %smoothing filter in the x?direction
gx=gx/sum(gx); % sum of weights equals one
gy=gx'; %smoothing filter in the y?direction (transpose of gx)

y=conv2(f,g,'valid'); %filter the image
figure(3);clf()
subplot(2,2,1); imshow(f/255); % original image
subplot(2,2,2); imshow(y/255); % smoothed image the 1D smoothing filter

yy=conv2(conv2(f,gx,'valid'),gy,'valid'); %filter the image
subplot(2,2,3);imshow(yy/255); %and display it
e=y-yy; %compute error
sum(sum(e.*e)) %print it on screen

Gx=fft(gx,32); %DFT of the filter
subplot(2,2,4);
stem(-16:15,fftshift(abs(Gx))); %display the magnitude spectrum

fs=backup_f(8:249,8:249); %extract subpart in the original image f (why 8:249?)
ffs=ff(8:249,8:249); %and in the noisy image ff
e=ffs-fs; sum(sum(e.*e)) %error between noisy and original image
e=yy-fs; sum(sum(e.*e)) %error between filtered and original image
