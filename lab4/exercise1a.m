clear
% close all

s=1.4; %standard deviation
[x,y]=meshgrid(-round(3*s):round(3*s),-round(3*s):round(3*s)); %sample grid
g=exp(-(x.*x + y.*y)/(2*s*s)); %2D smoothing filter
g=g/sum(sum(g)); % sum of weights equals one
figure(1); subplot(2,1,1);mesh(x,y,g); %show the filter
x1=-round(3*s):round(3*s); %sample grid
gx= exp(-(x1.*x1 )/(2*s*s)); %smoothing filter in the x?direction
gx=gx/sum(gx); % sum of weights equals one
gy=gx'; %smoothing filter in the y?direction (transpose of gx)
figure(1); subplot(2,1,2); stem(x1,gx); %show the 1D smoothing filter

f=double(imread('flowers.jpg')); %load the image
f=f(1:256,1:256); %crop it to 256 x 256 pixels
y=conv2(f,g,'valid'); %filter the image
figure(2);clf()
subplot(2,2,1); imshow(f/255); % original image
subplot(2,2,2); imshow(y/255); % smoothed image the 1D smoothing filter

yy=conv2(conv2(f,gx,'valid'),gy,'valid'); %filter the image
subplot(2,2,3);imshow(yy/255); %and display it
e=y-yy; %compute error
sum(sum(e.*e)) %print it on screen

Gx=fft(gx,32); %DFT of the filter
subplot(2,2,4); 
stem(abs(fftshift(Gx)));
% stem(-4:4,fftshift(abs(Gx))); %display the magnitude spectrum
