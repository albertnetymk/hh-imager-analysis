clear
close all
load char_e

se=ones(3,3); % 3 x 3 structuring element
char_ee =imerode(char_e,se);
ib=char_e & ~char_ee; % extract the ”inner boundary”, can be interpreted as the difference b3-b3e


figure(1)
subplot(2,2,1);imshow(char_e);
subplot(2,2,2);imshow(char_ee);
subplot(2,2,3); imshow(ib); axis on