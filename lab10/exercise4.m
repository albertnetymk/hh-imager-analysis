clear
close all
se=ones(3,3); % 3 x 3 structuring element
%Create the image b4
b4=zeros(32,32);
b4(6:26,12:20)=ones(21,9); %object
b4(18,15:16)=zeros(1,2); %small holes (noise)
b4(7:9,18)=zeros(3,1);
b4(27,27)=ones(1,1); %small objects (noise)
b4( 10:12,3)=ones(3,1);
subplot(2,2,1); imshow(b4); axis on; %display image

% subplot(2,2,2); imshow(imdilate(b4, se));
% closing = imerode(imdilate(b4, se), se);
% subplot(2,2,3); imshow(closing);


opening = imdilate(imerode(b4, se), se);
subplot(2,2,2); imshow(opening);


closing = imerode(imdilate(b4, se), se);
subplot(2,2,3); imshow(closing);


closing_opening = imdilate(imerode(closing, se), se);
subplot(2,2,4); imshow(closing_opening);