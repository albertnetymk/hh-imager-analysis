clear
close all
f = 1:5;  %JB: This is not known
F = fft(f)   %JB: This is not known
offset = 1; %JB: This is not known
f_shift = zeros(size(f));
for i=1:length(f)
    f_shift(i) = f(mod(i+offset-1, length(f))+1);
end

f_shift %JB: This is WHAT IS KNOWN IN A REAL APPLICATION

F_shift = fft(f_shift) %JB: This is KNOWN TOO...
F_shift_calculated = zeros(size(F_shift));
ratio = F(2)/F_shift(2) %JB: ...BUT HERE F(2) IS NOT KNOWN, so ratio is UNKNOWN!

for i=2:length(f_shift)
    F_shift_calculated(i) = F(i)/((ratio)^(i-1));
end
F_shift_calculated   