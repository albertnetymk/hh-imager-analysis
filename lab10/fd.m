clear
close all
f = 1:5;
F = fft(f)
offset = 1;
f_shift = zeros(size(f));
for i=1:length(f)
    f_shift(i) = f(mod(i+offset-1, length(f))+1);
end

F_shift = fft(f_shift)
F_shift_calculated = zeros(size(F_shift));
ratio = F(2)/F_shift(2)
for i=2:length(f_shift)
    F_shift_calculated(i) = F(i)/((ratio)^(i-1));
end
F_shift_calculated