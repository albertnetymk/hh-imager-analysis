clear
close all
s=1; %std in the spatial domain
x=-round(3*s):round(3*s); %sample grid
g1=exp(-(x.*x)/2/s/s); %smoothing filter
g1=g1/sum(g1); %gain=1

fmt=fmtest(256,[0.1*pi 0.3*pi]); %generate the image fmt
%generate the Gaussian pyramid
y256_s=conv2(conv2(fmt,g1,'same'),g1','same'); %first 2D smoothing
y128=shrink(y256_s,2); %then shrink by 2
y128_s=conv2(conv2(y128,g1,'same'),g1','same'); %first 2D smoothing
y64=shrink(y128_s,2); %then shrink by 2
y64_s=conv2(conv2(y64,g1,'same'),g1','same'); %first 2D smoothing
y32=shrink(y64_s,2); %then shrink by 2
%display the Gaussian pyramid in truesize

figure(1); imshow(fmt); truesize;
figure(2); imshow(y128); truesize;
figure(3); imshow(y64); truesize;
figure(4); imshow(y32);truesize;
% figure(1)
% clf()
% subplot(2,2,1); imshow(fmt);
% subplot(2,2,2); imshow(y128);
% subplot(2,2,3); imshow(y64);
% subplot(2,2,4); imshow(y32);