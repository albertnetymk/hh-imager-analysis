clear
close all
s=1.0; %std in the spatial domain
x=-round(4*s):round(4*s); %sample grid
g1=exp(-(x.*x)/2/s/s); %smoothing filter
g1=g1/sum(g1); %gain=1
g2=2*g1; %gain=2
figure(1); subplot(2,1,1);stem(x,g1); %show filters
subplot(2,1,2);stem(x,g2);

%Let us interpolate  a constant 1D function...it should result in a constant.
figure(2)
f=ones(1,30);y=conv(f,g1);subplot(4,1,1);stem(f);subplot(4,1,2);stem(y);
fz=zeros(1,60); fz(1:2:60)= f; fzi=conv(fz,g2);subplot(4,1,3);stem(fz);
subplot(4,1,4);stem(fzi);


fmt=fmtest(256, [0.1*pi 0.3*pi]); %generate the image fmt
figure(3); imshow(fmt); truesize; %and display in truesize
% What can you say about the frequency content of the image fmt?
% Without smoothing, use the function shrink to downsample the image fmt by a factor 2.
% Display the result image.
fmt_s2=shrink(fmt,2); %shrink by 2
figure(4);imshow(fmt_s2);truesize; %and display
% Explain the effect of downsampling without smoothing.





% Display the result image.
y=conv2(conv2(fmt,g1,'same'),g1','same'); % first 2D smoothing
fmtsmooth_s2=shrink(y,2); %then shrink by 2
figure(5);imshow(fmtsmooth_s2);truesize; %and display







fmt=fmtest(256,[0.1*pi 0.3*pi]); %generate the image fmt
%generate the Gaussian pyramid
y256_s=conv2(conv2(fmt,g1,'same'),g1','same'); %first 2D smoothing
y128=shrink(y256_s,2); %then shrink by 2
y128_s=conv2(conv2(y128,g1,'same'),g1','same'); %first 2D smoothing
y64=shrink(y128_s,2); %then shrink by 2
y64_s=conv2(conv2(y64,g1,'same'),g1','same'); %first 2D smoothing
y32=shrink(y64_s,2); %then shrink by 2
%display the Gaussian pyramid in truesize
figure(6); imshow(fmt); truesize;
figure(7); imshow(y128); truesize;
figure(8); imshow(y64); truesize;
figure(9); imshow(y32);truesize;



e128_1=expand(y128,2); %first expand
e128_2=conv2(conv2(e128_1,g2,'same'),g2','same'); %then interpolate
L256=fmt-e128_2; %take the difference
e64_1=expand(y64,2); %first expand
e64_2=conv2(conv2(e64_1,g2,'same'),g2','same'); %then interpolate
L128=y128-e64_2; %take the difference
e32_1=expand(y32,2); %first expand
e32_2=conv2(conv2(e32_1,g2,'same'),g2','same'); %then interpolate
L64=y64-e32_2; %take the difference
%display the Laplacian pyramid in truesize
figure(10); imagesc(L256); colormap(gray); truesize;
figure(11); imagesc(L128); colormap(gray); truesize;
figure(12); imagesc(L64); colormap(gray); truesize;


