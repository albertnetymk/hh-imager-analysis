function EI=expand(I,M)
EI=zeros(M*size(I,1),M*size(I,2));% We perepare EI first by filling everywhere with zeros.
           %Remember that the size of EI is dictated by size of I and M.     
EI(1:M:size(EI,1),1:M:size(EI,2))=I;  %%we expand the image with factor M using matlab...

return

