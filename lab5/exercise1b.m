clear
close all
s=1; %std in the spatial domain
x=-round(3*s):round(3*s); %sample grid
g1=exp(-(x.*x)/2/s/s); %smoothing filter
g1=g1/sum(g1); %gain=1

fmt=fmtest(256, [0.1*pi 0.3*pi]); %generate the image fmt
figure(2); imshow(fmt); truesize; %and display in truesize
fmt_s2=shrink(fmt,2); %shrink by 2
figure(3);imshow(fmt_s2);truesize; %and display
y=conv2(conv2(fmt,g1,'same'),g1','same'); % first 2D smoothing
fmtsmooth_s2=shrink(y,2); %then shrink by 2
figure(4);imshow(fmtsmooth_s2);truesize; %and display