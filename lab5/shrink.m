function SI=shrink(I,M)
Lrow=size(I,1);
Lcol=size(I,2);
SI=I(1:M:Lrow,1:M:Lcol); %%we shrink the image with factor M using matlab...

return

